package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByProjectId(String projectId);

    Task removeOneByProjectId(String projectId);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    List<Task> findAllTasksByProjectId (String projectId);

    int size();
}
