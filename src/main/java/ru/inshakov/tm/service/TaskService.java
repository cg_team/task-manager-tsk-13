package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.service.ITaskService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.model.Task;

import ru.inshakov.tm.util.ValidationUtil;

import java.util.List;

import static ru.inshakov.tm.util.ValidationUtil.isEmpty;

public class TaskService implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmpty(name)) return null;
        if (isEmpty(description)) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task findOneByName(final String name) {
        if (isEmpty(name)) return null;
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (ValidationUtil.checkIndex(index, taskRepository.size())) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return;
        remove(task);
    }

    @Override
    public void removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return;
        remove(task);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task removeOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (ValidationUtil.checkIndex(index, taskRepository.size())) return null;
        if (isEmpty(name)) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (isEmpty(id)) return null;
        if (isEmpty(name)) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
